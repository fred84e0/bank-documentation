# Get started

The following guide takes you through the process of getting the bank application up and running.


## Setup the project

If you have access to the projects repository and want to get the source code from gitlab please follow the steps.
( A gitlab account with access to the repository is provided in the report  )

* `git clone https://gitlab.com/fred84e0/dev-env-exam.git` - Clone the project (Use username and password provided in report to gain access).
* `cd dev-env-exam/` - Navigate to the project directory
* `docker login registry.gitlab.com/fred84e0/dev-env-exam/bankapp:latest` - Login to access docker image with (With the provided login details from the report)
* `RTE=dev docker-compose up` - Runs the application on 0.0.0.0:8000

!!! info

    You might want to add the Gitlab user to the repository <br />
    `git config user.name <Gitlab Username>` <br />
    `git config user.password <Gitlab password>` <br />
    Please <strong>DO NOT</strong> push anything to the repository

## Demo users

### Admin user
A provisioning script will add a superuser to the database which can be used to login to the admin page

* Bank application: [0.0.0.0:8000/admin](0.0.0.0:8000/admin)
* Username: adminuser
* Password: adminpassword123


## Two factor authentication

Since the project uses two factor authentication with django-otp an authenticator app is needed to scan qr-codes and receive one time passwords
Please download <strong> Google authenticator app </strong> or similar to be able to login