# Introduction

Project created by Frederik Frost Jensen

This documentation site will give you the needed info in order to try the bank application.
This site is part of an exam project on KEA Web development. (Development environment course)


## Repositories

* Bank application: [https://gitlab.com/fred84e0/dev-env-exam](https://gitlab.com/fred84e0/dev-env-exam)
* Documentation repository: [https://gitlab.com/fred84e0/bank-documentation](https://gitlab.com/fred84e0/bank-documentation)
